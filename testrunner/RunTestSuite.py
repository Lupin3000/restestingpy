# -*- coding: utf8 -*-
"""Missing docstring"""

import logging
import ast
import binascii
import os
from testrunner.reports.ReportPlain import ReportPlain
from testrunner.reports.ReportJson import ReportJson
from testrunner.reports.ReportXml import ReportXml
from testrunner.connectors.HttpRequest import HttpRequest
from testrunner.validation.ResponseStatus import ResponseStatus
from testrunner.validation.ResponseHeader import ResponseHeader
from testrunner.validation.ResponseBody import ResponseBody
from testrunner.validation.ResponseTime import ResponseTime


class RunTestSuite(object):
    """Missing docstring"""

    def __init__(self, configuration, host, testsuite, options):
        """
        Initialize properties

        @type configuration: dict
        @param configuration: some configuration dict
        @type host: string
        @param host: some host string
        @type testsuite: list
        @param testsuite: some testsuite list
        @type options: list
        @param options: some options list
        """
        self.__logger = logging.getLogger(__name__)

        self.__configuration = configuration
        self.__host = host
        self.__suite = testsuite
        self.__options = options
        self.test_id = 0
        self.test_status = [0, 0, 0]

        # create report object
        if options[0] == 'xml':
            self.__report = ReportXml(self.__host,
                                      self.__configuration.get('title'))
        elif options[0] == 'json':
            self.__report = ReportJson(self.__host,
                                       self.__configuration.get('title'))
        else:
            self.__report = ReportPlain(self.__host,
                                        self.__configuration.get('title'))

    def __skipped_test(self, testcase_id):
        """
        Add skipped testcase

        @type testcase_id: int
        @param testcase_id: some testcase_id int
        """
        self.test_status[0] += 1
        self.__report.set_testcase_status(int(testcase_id), 'skipped')

    def __failed_test(self, testcase_id, assertion, message):
        """
        Add failed testcase

        @type testcase_id: int
        @param testcase_id: some testcase_id int
        @type assertion: string
        @param assertion: some assertion string
        @type message: string
        @param message: some message string
        """
        self.test_status[1] += 1
        self.__report.set_testcase_status(
            int(testcase_id), 'failed', str(assertion), str(message))

    def __passed_test(self, testcase_id):
        """
        Add passed testcase

        @type testcase_id: int
        @param testcase_id: some testcase_id int
        """
        self.test_status[2] += 1
        self.__report.set_testcase_status(int(testcase_id), 'passed')

    def __prepare_header(self):
        """
        Prepare request header

        @return: dict
        """
        value = self.__configuration.get('header')
        header = ast.literal_eval(value)

        if self.__suite[self.test_id][4]:
            # read from sqlite
            header.update(ast.literal_eval(self.__suite[self.test_id][4]))

        return header

    def __http_transaction(self):
        """
        Run testcase http transaction

        @return: string, class, float
        """
        url = str(self.__host) + str(self.__suite[self.test_id][7])
        method = str(self.__suite[self.test_id][3]).upper()
        payload = self.__suite[self.test_id][5]

        if self.__options[3] and method == 'GET':
            hash_string = str(binascii.hexlify(os.urandom(16)))
            random_query = 'random_query=' + hash_string

            if '?' in url:
                url += '&' + random_query
            else:
                url += '?' + random_query

        run = HttpRequest(url, method, self.__options[1], self.__options[2])
        run.set_header(self.__prepare_header())
        run.set_payload(payload)
        response, duration = run.make_request()

        return url, response, duration

    def __check_response(self, url, response, duration):
        """
        Check response status, header, body and duration

        @type url: string
        @param url: target url
        @type response: class
        @param response: some response class
        @type duration: float
        @param duration: some duration float
        """
        test_results = list()
        assertion = message = ''
        needed_time = float(duration)
        namespace = ast.literal_eval(self.__configuration.get('namespace'))

        # set max round trip time
        if self.__suite[self.test_id][12]:
            max_time = float(self.__suite[self.test_id][12])
        else:
            max_time = float(self.__configuration.get('time'))

        # check response status code - test_results[0]
        test_results.append(
            ResponseStatus.is_status_code(
                response.status_code, self.__suite[self.test_id][8]))

        if not test_results[0]:
            assertion, message = ResponseStatus.get_error()

        # check response header - test_results[1]
        if test_results[0]:
            test_results.append(ResponseHeader.is_header(
                response.headers, self.__suite[self.test_id][9]))
        else:
            test_results.append(True)

        if not test_results[1]:
            assertion, message = ResponseHeader.get_error()

        # check response body - test_results[2]
        if test_results[0] and test_results[1]:
            test_results.append(ResponseBody.is_body(
                response, self.__suite, self.test_id,
                self.__configuration, namespace))
        else:
            test_results.append(True)

        if not test_results[2]:
            assertion, message = ResponseBody.get_error()
            message += '\t\n ---- \n'
            message += response.text.encode('utf-8') + '\n'

        # check duration - test_results[3]
        if test_results[0] and test_results[1] and test_results[2]:
            test_results.append(
                ResponseTime.is_duration(needed_time, max_time))
        else:
            test_results.append(True)

        if not test_results[3]:
            assertion, message = ResponseTime.get_error()

        # set failed or passed
        if False in test_results:
            message += ' - "' + url + '"'
            self.__failed_test(self.__suite[self.test_id][0],
                               assertion, message)
        else:
            self.__passed_test(self.__suite[self.test_id][0])

    def __check_testcase(self):
        """
        Check if current testcase values are valid

        @return: bool
        """
        self.__logger.debug('check testcase: %d',
                            self.__suite[self.test_id][0])

        check = list()
        if not self.__suite[self.test_id][0]:
            check.append(False)
        else:
            check.append(True)

        if self.__suite[self.test_id][1] != 'y':
            check.append(False)
        else:
            check.append(True)

        if not self.__suite[self.test_id][3]:
            check.append(False)
        else:
            check.append(True)

        if not self.__suite[self.test_id][8]:
            check.append(False)
        else:
            check.append(True)

        if False in check:
            return False
        else:
            return True

    def run_tests(self):
        """
        Run all testcases of testsuite
        """
        self.__logger.debug('start running testsuite\n')

        # iterate testsuite list
        while self.test_id < len(self.__suite):
            # check for skipped testcase
            if self.__check_testcase():
                self.__logger.info('** running testcase %d **',
                                   self.__suite[self.test_id][0])

                url, response, duration = self.__http_transaction()
                self.__check_response(url, response, duration)
            else:
                self.__logger.info('** skipping testcase %d **',
                                   self.__suite[self.test_id][0])

                self.__skipped_test(self.__suite[self.test_id][0])

            # increment for while loop
            self.test_id += 1

    def show_report(self):
        """
        Print report to stdout
        """
        self.__logger.info('** prepare and show test report **')

        self.__report.set_report_header(self.test_status)
        self.__report.set_report_footer()
        print self.__report.get_report()
