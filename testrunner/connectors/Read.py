# -*- coding: utf8 -*-
"""Missing docstring"""

import logging


class Read(object):
    """Missing docstring"""

    _logger = logging.getLogger(__name__)
    __message = ''

    @staticmethod
    def set_error(message):
        """
        Set value for property

        @type message: string
        @param message: some message string
        """
        Read.__message = str(message)

    @staticmethod
    def get_error():
        """
        Return value of property

        @return: string
        """
        return Read.__message
