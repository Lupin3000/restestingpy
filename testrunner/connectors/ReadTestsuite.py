# -*- coding: utf8 -*-
"""Missing docstring"""

import sqlite3
from testrunner.connectors.Read import Read


class ReadTestsuite(Read):
    """Missing docstring"""

    __testsuite = []

    @staticmethod
    def get_testsuite():
        """
        Return testsuite content as list

        @return: list
        """
        return ReadTestsuite.__testsuite

    @staticmethod
    def read_sqlite_file(filename):
        """
        Read testsuite from sqlite file

        @type filename: string
        @param filename: some filename string
        @return: bool
        """
        ReadTestsuite._logger.info('read testsuite: %s', filename)

        connection = None

        try:
            connection = sqlite3.connect(filename)
            cursor = connection.cursor()
            cursor.execute('SELECT * FROM testsuite')
            ReadTestsuite.__testsuite = cursor.fetchall()
        except sqlite3.Error:
            ReadTestsuite._logger.error('read file', exc_info=True)
            return False
        finally:
            connection.close()

        ReadTestsuite._logger.debug('%s', ReadTestsuite.__testsuite)

        return True
