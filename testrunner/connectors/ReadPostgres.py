# -*- coding: utf8 -*-
"""Missing docstring"""

import sys
import psycopg2
from testrunner.connectors.Read import Read


class ReadPostgres(Read):
    """Missing docstring"""

    def __init__(self, db_credentials):
        """
        Connect to postgres

        @type db_credentials: dictionary
        @param db_credentials: some db_credentials dic
        """
        ReadPostgres._logger.debug('start db connection')

        dbname = str(db_credentials.get('dbname'))
        dbhost = str(db_credentials.get('dbhost'))
        dbport = int(db_credentials.get('dbport'))
        dbuser = str(db_credentials.get('dbuser'))
        dbpasswd = str(db_credentials.get('dbpasswd'))

        try:
            self.connection = psycopg2.connect(host=dbhost, port=dbport,
                                               database=dbname, user=dbuser,
                                               password=dbpasswd)
            self.cursor = self.connection.cursor()
        except psycopg2.Error:
            ReadPostgres._logger.critical('Unable to connect', exc_info=True)
            sys.exit(1)

    def run_query(self, sql_query):
        """
        Execute SQL query

        @type sql_query: string
        @param sql_query: some sql_query string
        @return: tuple|list
        """
        ReadPostgres._logger.debug('run sql query')

        try:
            self.cursor.execute(sql_query)
            result = self.cursor.fetchall()
        except psycopg2.Error:
            ReadPostgres._logger.critical('Unable to query', exc_info=True)
            result = []

        return result

    def close_connection(self):
        """
        Close postgres connection
        """
        ReadPostgres._logger.debug('close db connection')

        self.cursor.close()
        self.connection.close()
