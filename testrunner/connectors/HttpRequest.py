# -*- coding: utf8 -*-
"""Missing docstring"""

import logging

import requests


class HttpRequest(object):
    """Missing docstring"""

    __logger = logging.getLogger(__name__)

    def __init__(self, url, method, verify, follow):
        """Missing docstring"""
        # disable package warnings
        requests.packages.urllib3.disable_warnings()

        self.__payload = None
        self.__headers = None
        self.__files = None
        self.__url = url
        self.__method = method
        self.__ssl_verify = verify
        self.__follow = follow

    def set_header(self, header):
        """
        Set header for request

        @type header: dict
        @param header: some header dict
        """
        self.__headers = header

    def set_payload(self, payload):
        """
        Set payload for request

        @type payload: dict
        @param payload: some payload dict
        """
        self.__payload = payload

    def set_files(self, files):
        """
        Set files for request

        @type files: dict
        @param files: some files dict
        """
        self.__files = files

    def make_request(self):
        """Missing docstring"""
        self.__logger.info('%s \'%s\' HTTP/1.1', self.__method, self.__url)
        self.__logger.info(str(self.__headers))
        self.__logger.info(str(self.__payload))

        ses = requests.Session()
        prepped = requests.Request(self.__method,
                                   self.__url,
                                   headers=self.__headers,
                                   data=self.__payload).prepare()

        response = ses.send(prepped,
                            verify=self.__ssl_verify,
                            allow_redirects=self.__follow)

        self.__logger.info('Status: %s', response.status_code)
        self.__logger.debug('Encoding: %s', response.encoding)
        self.__logger.debug('Header: %s', response.headers)
        self.__logger.debug('History: %s', response.history)
        self.__logger.debug('Body: %s', response.text)

        # get time total in seconds
        time_delta = response.elapsed
        duration = time_delta.total_seconds()
        self.__logger.debug('Duration: %s', duration)

        return response, duration
