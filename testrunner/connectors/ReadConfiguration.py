# -*- coding: utf8 -*-
"""Missing docstring"""

import ConfigParser
from testrunner.connectors.Read import Read


class ReadConfiguration(Read):
    """Missing docstring"""

    __configuration = {}

    @staticmethod
    def get_configuration():
        """
        Return configuration content as dictionary

        @return: dictionary
        """
        return ReadConfiguration.__configuration

    @staticmethod
    def read_configuration_file(filename):
        """
        Read configuration from *.ini file

        @type filename: string
        @param filename: some filename string
        @return: bool
        """
        ReadConfiguration._logger.info('read configuration: %s', filename)

        parser = ConfigParser.SafeConfigParser()
        parser.read(filename)

        try:
            ReadConfiguration.__configuration['title'] = parser.get(
                'global', 'title')
            ReadConfiguration.__configuration['header'] = parser.get(
                'global', 'default')
            ReadConfiguration.__configuration['time'] = parser.get(
                'global', 'time')
            ReadConfiguration.__configuration['namespace'] = parser.get(
                'xml', 'namespace')
            ReadConfiguration.__configuration['xpath'] = parser.get(
                'xml', 'xpath')
            ReadConfiguration.__configuration['dbtype'] = parser.get(
                'database', 'dbtype')
            ReadConfiguration.__configuration['dbname'] = parser.get(
                'database', 'dbname')
            ReadConfiguration.__configuration['dbhost'] = parser.get(
                'database', 'dbhost')
            ReadConfiguration.__configuration['dbport'] = parser.get(
                'database', 'dbport')
            ReadConfiguration.__configuration['dbuser'] = parser.get(
                'database', 'dbuser')
            ReadConfiguration.__configuration['dbpasswd'] = parser.get(
                'database', 'dbpasswd')
            ReadConfiguration._logger.debug(
                '%s', ReadConfiguration.__configuration)
            return True
        except ConfigParser.Error:
            ReadConfiguration._logger.error('read file', exc_info=True)
            return False
