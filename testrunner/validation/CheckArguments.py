# -*- coding: utf8 -*-
"""Missing docstring"""

import logging
import socket
import urlparse
import httplib

from testrunner.validation.CheckFile import CheckFile


class CheckArguments(object):
    """Check user arguments"""

    __logger = logging.getLogger(__name__)

    @staticmethod
    def is_empty_string(item):
        """
        Check for item empty string

        @type item: string
        @param item: some string
        @return: bool
        """
        CheckArguments.__logger.debug('check empty: %s', item)

        if item:
            return True
        else:
            CheckArguments.__logger.error('empty string')
            return False

    @staticmethod
    def is_reachable(target):
        """
        Check if target host/ip reachable

        @type target: string
        @param target: some url string
        @return: bool
        """
        CheckArguments.__logger.debug('check reachable: %s', target)

        socket.setdefaulttimeout(10)
        host, path = urlparse.urlparse(target)[1:3]

        try:
            conn = httplib.HTTPConnection(host)
            conn.request('HEAD', path)
            status = conn.getresponse().status
            del status
            return True
        except socket.timeout:
            CheckArguments.__logger.error('Failed to connect', exc_info=True)
            return False

    @staticmethod
    def is_output_value(report):
        """
        Check if report output matches allowed values

        @type report: string
        @param report: some report string
        @return: bool
        """
        CheckArguments.__logger.debug('check allowed string: %s', report)

        if report.lower() in ('default', 'json', 'xml'):
            return True
        else:
            CheckArguments.__logger.error('argument for report is %s', report)
            return False

    @staticmethod
    def is_allowed_testsuite(testsuite):
        """
        Check if testsuite extension matches allowed values

        @type testsuite: string
        @param testsuite: some testsuite string
        @return: bool
        """
        CheckArguments.__logger.debug('check allowed extension: %s', testsuite)

        allowed_ext = ['db', 'sqlite', 'sqlite3', 'db3']
        is_ext = CheckFile.get_file_extension(testsuite)[1:].lower()

        if is_ext not in allowed_ext:
            CheckArguments.__logger.error('wrong extension %s', is_ext)
            return False
        else:
            return True
