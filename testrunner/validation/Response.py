# -*- coding: utf8 -*-
"""Missing docstring"""

import logging


class Response(object):
    """Response validation"""

    _logger = logging.getLogger(__name__)
    _assertion = ''
    _message = ''

    @staticmethod
    def set_error(assertion, message):
        """
        Set assertion and message value

        @type assertion: string
        @param assertion: some assertion string
        @type message: string
        @param message: some message string
        """
        Response._assertion = str(assertion)
        Response._message = str(message)

    @staticmethod
    def get_error():
        """
        Return properties assertion and message as string

        @return: string, string
        """
        return Response._assertion, Response._message
