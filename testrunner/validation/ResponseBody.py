# -*- coding: utf8 -*-
"""Missing docstring"""

import json
import StringIO
import re
import jsonpath
from lxml import etree
from testrunner.validation.Response import Response
from testrunner.connectors.ReadPostgres import ReadPostgres


class ResponseBody(Response):
    """Response body validation"""

    __database = None
    __namespace = ''
    __xpath = ''

    @staticmethod
    def __get_xpath(item):
        """
        Return xpath as string

        @type item: string
        @param item: some item string
        @return: string
        """
        # create item string
        string = str(item)

        # create xpath 2.0
        str_xpath = ResponseBody.__xpath.replace('%STRING%', string)

        ResponseBody._logger.debug('xpath: %s', str_xpath)

        # return xpath as string
        return str_xpath

    @staticmethod
    def __get_query_result(query):
        """
        Return query result as tuple

        @type query: string
        @param query: some sql query string
        @return: tuple
        """

        sql = ReadPostgres(ResponseBody.__database)
        result = sql.run_query(str(query))
        sql.close_connection()

        ResponseBody._logger.debug(result)
        return result

    @staticmethod
    def _is_type(item):
        """
        Check if validation_type item is in allowed list

        @type item: string
        @param item: some item string
        @return: bool
        """
        allowed_types = ['compare', 'regex', 'jsonpath',
                         'xpath', 'sql-regex', 'sql-xpath', "json-with-sort"]

        if str(item.lower()) in allowed_types:
            return True
        else:
            message = 'validation_type %s not supported' % item
            ResponseBody._logger.debug(message)
            Response.set_error('ERROR: testcase', message)
            return False

    @staticmethod
    def _is_assertion(item):
        """
        Check if assertion item is set and not empty

        @type item: string
        @param item: some item string
        @return: bool
        """
        if item:
            return True
        else:
            message = 'assertion not set'
            ResponseBody._logger.debug(message)
            Response.set_error('ERROR: testcase', message)
            return False

    @staticmethod
    def _is_compare_assertion(assertion, response):
        """
        Compare assertion string again response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running compare assertion')

        if assertion == response.text:
            return True
        else:
            Response.set_error('ERROR: compare',
                               'compare assertion for body fails')
            return False

    @staticmethod
    def _is_regex_assertion(assertion, response):
        """
        Check with regex if assertion is in response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running regex assertion')

        regex = r"" + re.escape(str(assertion)) + r""
        if re.search(regex, response.text, re.MULTILINE):
            return True
        else:
            ResponseBody._logger.info('regex: %s', regex)
            Response.set_error('ERROR: regex',
                               'regex assertion for body fails')
            return False

    @staticmethod
    def _is_jsonpath_assertion(assertion, response):
        """
        Check with jsonpath if assertion is in response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running jsonpath assertion')

        json_path = assertion.splitlines()[0]
        pattern = assertion.splitlines()[1]
        json_string = json.loads(response.text)
        match = jsonpath.jsonpath(json_string, json_path)

        if not match:
            ResponseBody._logger.info('jsonpath: %s', json_path)
            Response.set_error('ERROR: jsonpath',
                               'jsonpath assertion for body fails')
            return False

        if pattern in map(str, match):
            return True
        else:
            ResponseBody._logger.info('jsonpath: %s', json_path)
            Response.set_error('ERROR: jsonpath',
                               'jsonpath assertion for body fails')
            return False

    @staticmethod
    def _is_xpath_assertion(assertion, response):
        """
        Check with xpath if assertion is in response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running xpath assertion')

        # create xml from response
        xml_string = StringIO.StringIO(response.content)
        tree = etree.parse(xml_string)

        if tree.xpath(assertion, namespaces=ResponseBody.__namespace):
            return True
        else:
            ResponseBody._logger.info('xpath: %s', assertion)
            Response.set_error('ERROR: xpath',
                               'xpath assertion for body fails')
            return False

    @staticmethod
    def _is_sql_xpath_assertion(assertion, response):
        """
        Check with sql-xpath if assertion is in response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running sql-xpath assertion')

        error = 0

        # prepare xml tree
        xml_string = StringIO.StringIO(response.content)
        tree = etree.parse(xml_string)

        # sql
        sql_result = ResponseBody.__get_query_result(str(assertion))

        # verification for each database result
        for tuple_item in sql_result:
            for item in tuple_item:
                str_path = ResponseBody.__get_xpath(item)
                if not tree.xpath(
                        str_path, namespaces=ResponseBody.__namespace):
                    ResponseBody._logger.debug(str_path)
                    error += 1

        # check for error
        if error == 0:
            return True
        else:
            ResponseBody._logger.info('sql-xpath fails')
            Response.set_error('ERROR: sql-xpath',
                               'sql-xpath assertion for body fails')
            return False

    @staticmethod
    def _is_sql_regex_assertion(assertion, response):
        """
        Check with sql-regex if assertion is in response

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running sql-regex assertion')

        error = 0

        # sql
        sql_result = ResponseBody.__get_query_result(str(assertion))

        # verification for each database result
        for tuple_item in sql_result:
            for item in tuple_item:
                regex = r"" + re.escape(str(item)) + r""
                if not re.search(regex, response.text, re.MULTILINE):
                    ResponseBody._logger.info('sql-regex: %s', regex)
                    Response.set_error('ERROR: sql-regex',
                                       'sql-regex assertion for body fails')
                    error += 1

        # check for error
        if error == 0:
            return True
        else:
            return False

    @staticmethod
    def _is_json_with_sort_assertion(assertion, response):
        """
        Check with if the assertion is the same as response if they are
        treated as jsons and we disregard order

        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        ResponseBody._logger.info('running json-with-sort assertion')

        # sql
        json_result = json.loads(response.text)
        json_expected = json.loads(assertion)

        ordered_json_result = ResponseBody._ordered(json_result)
        ordered_json_expected = ResponseBody._ordered(json_expected)

        if ordered_json_result == ordered_json_expected:
            return True
        else:
            Response.set_error('ERROR: json-with-sort',
                               'json with sort for body fails')
            return False

    @staticmethod
    def _ordered(obj):
        if isinstance(obj, dict):
            return sorted((k, ResponseBody._ordered(v)) for k, v in obj.items())
        if isinstance(obj, list):
            return sorted(ResponseBody._ordered(x) for x in obj)
        else:
            return obj

    @staticmethod
    def _run_validation(validation_type, assertion, response):
        """
        Run assertion on response by validation_type

        @type validation_type: string
        @param validation_type: some validation_type string
        @type assertion: string
        @param assertion: some assertion string
        @type response: class
        @param response: some response class
        @return: bool
        """
        validation_type = str(validation_type.lower())

        if validation_type == 'compare':
            result = ResponseBody._is_compare_assertion(assertion, response)
        elif validation_type == 'regex':
            result = ResponseBody._is_regex_assertion(assertion, response)
        elif validation_type == 'jsonpath':
            result = ResponseBody._is_jsonpath_assertion(assertion, response)
        elif validation_type == 'xpath':
            result = ResponseBody._is_xpath_assertion(assertion, response)
        elif validation_type == 'sql-xpath':
            result = ResponseBody._is_sql_xpath_assertion(assertion, response)
        elif validation_type == 'sql-regex':
            result = ResponseBody._is_sql_regex_assertion(assertion, response)
        elif validation_type == 'json-with-sort':
            result = ResponseBody._is_json_with_sort_assertion(
                assertion, response)
        else:
            result = False

        return result

    @staticmethod
    def is_body(response, testsuite, testsuite_id, configuration, namespace):
        """
        Prepare response body validation

        @type response: class
        @param response: some response class
        @type testsuite: list
        @param testsuite: some testsuite list
        @type testsuite_id: int
        @param testsuite_id: some testsuite int
        @type configuration: dict
        @param configuration: some configuration dict
        @type namespace: string
        @param namespace: some namespace string
        @return: bool
        """
        ResponseBody._logger.debug('check response body')

        ResponseBody.__database = configuration
        ResponseBody.__namespace = namespace
        ResponseBody.__xpath = configuration.get('xpath')

        if not testsuite[testsuite_id][10]:
            return True
        else:
            check_bool = True

            # check validation type
            if not ResponseBody._is_type(testsuite[testsuite_id][10]):
                check_bool = False

            # check validation
            if not ResponseBody._is_assertion(testsuite[testsuite_id][11]):
                check_bool = False

            # run validation
            if check_bool:
                check_bool = ResponseBody._run_validation(
                    testsuite[testsuite_id][10],
                    testsuite[testsuite_id][11],
                    response)

            return check_bool
