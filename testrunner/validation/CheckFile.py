# -*- coding: utf8 -*-
"""Missing docstring"""

import logging

import os


class CheckFile(object):
    """Check file"""

    __logger = logging.getLogger(__name__)

    @staticmethod
    def get_file_size(filename):
        """
        Get file size

        @type filename: string
        @param filename: some filename string
        @return: string|bool
        """
        CheckFile.__logger.debug('Get file size: %s', filename)

        try:
            return os.stat(filename).st_size
        except OSError:
            CheckFile.__logger.error('cannot get size', exc_info=True)
            return False

    @staticmethod
    def is_file_readable(filename):
        """
        Check for file is readable

        @type filename: string
        @param filename: some filename string
        @return: bool
        """
        CheckFile.__logger.debug('check readable: %s', filename)

        try:
            return os.access(filename, os.R_OK)
        except OSError:
            CheckFile.__logger.error('file not readable', exc_info=True)
            return False

    @staticmethod
    def get_file_extension(filename):
        """
        Extract filename extension

        @type filename: string
        @param filename: some filename string
        @return: string|bool
        """
        CheckFile.__logger.debug('get extension: %s', filename)

        try:
            return os.path.splitext(filename)[1]
        except OSError:
            CheckFile.__logger.error('extension missing', exc_info=True)
            return False
