# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.validation.Response import Response


class ResponseHeader(Response):
    """Response header validation"""

    _item = ''

    @staticmethod
    def _validate_header_element(response, item):
        """Missing docstring"""
        if item in response.values():
            return True
        else:
            ResponseHeader._logger.debug(
                'did not found header item: %s', item)
            ResponseHeader._item = item
            return False

    @staticmethod
    def is_header(response, assert_header):
        """
        Check response header

        @type response: dict
        @param response: some response dict
        @type assert_header: string
        @param assert_header: some assert_header string
        @return: bool
        """
        ResponseHeader._logger.debug('check response header')

        result = list()

        if not assert_header:
            return True
        else:
            expected_list = assert_header.split('\n')
            for element in expected_list:
                result.append(ResponseHeader._validate_header_element(
                    response, element))

        if False in result:
            ResponseHeader._logger.debug('response header wrong')
            message = 'Header item %s not found' % ResponseHeader._item
            Response.set_error(
                'ERROR: Response Header', message)
            return False
        else:
            return True
