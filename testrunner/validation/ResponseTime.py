# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.validation.Response import Response


class ResponseTime(Response):
    """Round trip validation"""

    @staticmethod
    def is_duration(duration, max_time):
        """
        Compare round trip duration again assertion

        @type duration: float
        @param duration: some duration float
        @type max_time: float
        @param max_time: some time float
        @return: bool
        """
        ResponseTime._logger.debug('check round trip duration')

        if duration <= float(max_time):
            return True
        else:
            ResponseTime._logger.debug(
                'duration time is %s not %s', duration, max_time)
            assertion = 'ERROR: Duration should %s' % max_time
            message = 'Duration is %s' % duration
            Response.set_error(assertion, message)
            return False
