# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.validation.Response import Response


class ResponseStatus(Response):
    """Response status code validation"""

    @staticmethod
    def is_status_code(response_status_code, assert_status_code):
        """
        Check for expected status code

        @type response_status_code: int
        @param response_status_code: some status code int
        @type assert_status_code: int
        @param assert_status_code: some status code int
        @return: bool
        """
        ResponseStatus._logger.debug('check response status code')

        res_status = int(response_status_code)
        ass_status = int(assert_status_code)

        if res_status == ass_status:
            return True
        else:
            ResponseStatus._logger.debug(
                'response status is %d not %d', res_status, ass_status)
            assertion = 'ERROR: Status should %d' % ass_status
            message = 'Status is %d' % res_status
            Response.set_error(assertion, message)
            return False
