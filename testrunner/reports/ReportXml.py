# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.reports.Report import Report


class ReportXml(Report):
    """Create XML report"""

    def set_testcase_status(self, test_id, status, assertion='', message=''):
        """
        Create xml report for testcase

        @type test_id: int
        @param test_id: some test_id int
        @type status: string
        @param status: specific status string (passed, skipped, failed)
        @type assertion: string
        @param assertion: optional assertion string
        @type message: string
        @param message: optional message string
        """
        ReportXml._logger.debug('add testcase status as xml')

        self._test_report += "\n\t\t<testcase name=\"id_%d\">" % test_id

        if status == 'skipped':
            self._test_report += "\n\t\t\t<skipped/>"

        if status == 'failed':
            self._test_report += "\n\t\t\t<error message=\"Assertion Error"
            self._test_report += ": %s\">" % assertion
            self._test_report += "\n\t\t\t\t<![CDATA["
            self._test_report += "\n\t\t\t\t%s" % message
            self._test_report += "\n\t\t\t\t]]>"
            self._test_report += "\n\t\t\t</error>"

        self._test_report += "\n\t\t</testcase>"

    def set_report_header(self, status_list):
        """
        Create report xml header

        @type status_list: list
        @param status_list: some status_list list
        """
        ReportXml._logger.debug('set report header as xml')

        test_cases = self._test_report
        total = status_list[0] + status_list[1] + status_list[2]

        header_str = '<?xml version="1.0" encoding="UTF-8"?>\n'
        header_str += '<testsuites>\n'
        header_str += '\t<testsuite name=\"%s\"' % self._suite
        header_str += ' tests=\"%d\" skipped=\"%d\" errors=\"%d\"' % (
            total, status_list[0], status_list[1])
        header_str += ' hostname=\"%s\" timestamp=\"%sT%s\">' % (
            self._host, self.get_report_date(), self.get_report_time())
        self._test_report = header_str
        self._test_report += test_cases

    def set_report_footer(self):
        """
        Create report xml footer
        """
        ReportXml._logger.debug('set report footer as xml')

        self._test_report += '\n\t</testsuite>\n</testsuites>'
