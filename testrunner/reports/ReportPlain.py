# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.reports.Report import Report


class ReportPlain(Report):
    """Create text plain report"""

    def set_testcase_status(self, test_id, status, assertion='', message=''):
        """
        Create text plain report for testcase

        @type test_id: int
        @param test_id: some test_id int
        @type status: string
        @param status: specific status string (passed, skipped, failed)
        @type assertion: string
        @param assertion: optional assertion string
        @type message: string
        @param message: optional message string
        """
        ReportPlain._logger.debug('add testcase status as plain text')

        self._test_report += '\n-- Testcase ID: %d --' % test_id
        self._test_report += '\nStatus: %s' % status

        if status == 'failed':
            self._test_report += '\nAssertion: %s' % assertion
            self._test_report += '\nMessage: %s' % message

        self._test_report += '\n'

    def set_report_header(self, status_list):
        """
        Create report text plain header

        @type status_list: list
        @param status_list: some status_list list
        """
        ReportPlain._logger.debug('set report header as plain text')

        test_cases = self._test_report
        total = status_list[0] + status_list[1] + status_list[2]

        header_str = '\n-- Overview ' + '-' * 70 + '\n\n'
        header_str += 'Suite: %s \n' % self._suite
        header_str += 'Host: %s \n' % self._host
        header_str += 'Date: %s \n' % self.get_report_date()
        header_str += 'Time: %s \n' % self.get_report_time()
        header_str += 'Total: %d\n' % total
        header_str += 'Skipped: %d - Failed: %d - Passed: %d\n' % (
            status_list[0], status_list[1], status_list[2])
        self._test_report = header_str
        self._test_report += test_cases

    def set_report_footer(self):
        """
        Create report text plain footer
        """
        ReportPlain._logger.debug('set report footer as plain text')

        footer_str = '\n-- Finish ' + '-' * 72 + '\n'
        self._test_report += footer_str
