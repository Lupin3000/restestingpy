# -*- coding: utf8 -*-
"""Missing docstring"""

import logging
import time


class Report(object):
    """Create report"""

    _logger = logging.getLogger(__name__)

    def __init__(self, host, suite):
        """
        Define properties

        @type host: string
        @param host: some host string
        @type suite: string
        @param suite: some suite string
        """
        self._test_report = ''
        self._host = host
        self._suite = suite

    def get_report(self):
        """
        Return report as string

        @return: string
        """
        self._logger.debug('print test report')

        return self._test_report

    @staticmethod
    def get_report_date():
        """
        Return current date string as string MM/DD/YYYY

        @return: string
        """
        return time.strftime("%d/%m/%Y")

    @staticmethod
    def get_report_time():
        """
        Return current time as string HH:MM:SS

        @return: string
        """
        return time.strftime("%X")
