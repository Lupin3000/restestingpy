# -*- coding: utf8 -*-
"""Missing docstring"""

from testrunner.reports.Report import Report


class ReportJson(Report):
    """Create JSON report"""

    def set_testcase_status(self, test_id, status, assertion='', message=''):
        """
        Create json report for testcase

        @type test_id: int
        @param test_id: some test_id int
        @type status: string
        @param status: specific status string (passed, skipped, failed)
        @type assertion: string
        @param assertion: optional assertion string
        @type message: string
        @param message: optional message string
        """
        ReportJson._logger.debug('add testcase status as json')

        self._test_report += "\n\t\t{"
        self._test_report += "\n\t\t\t\"id\":%d," % test_id
        self._test_report += "\n\t\t\t\"status\":%s" % status

        if status == 'failed':
            self._test_report += ",\n\t\t\t\"assertion\":\"%s\"" % assertion
            self._test_report += ",\n\t\t\t\"message\":\"%s\"" % message

        self._test_report += "\n\t\t},"

    def set_report_header(self, status_list):
        """
        Create report json header

        @type status_list: list
        @param status_list: some status_list list
        """
        ReportJson._logger.debug('set report header as json')

        test_cases = self._test_report
        total = status_list[0] + status_list[1] + status_list[2]

        header_str = '{'
        header_str += '\n\t\"name\":\"%s\",' % self._suite
        header_str += '\n\t\"date\":\"%s\",' % self.get_report_date()
        header_str += '\n\t\"time\":\"%s\",' % self.get_report_time()
        header_str += '\n\t\"host\":\"%s\",' % self._host
        header_str += '\n\t\"tests\":%d,' % total
        header_str += '\n\t\"skipped\":%d,' % status_list[0]
        header_str += '\n\t\"failed\":%d,' % status_list[1]
        header_str += '\n\t\"passed\":%d,' % status_list[2]
        header_str += "\n\t\"results\":["
        self._test_report = header_str
        self._test_report += test_cases

    def set_report_footer(self):
        """
        Create report json footer
        """
        ReportJson._logger.debug('set report footer as json')

        self._test_report = self._test_report[:-1] + '\n\t]\n}'
