BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS testsuite (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    run CHAR(1) NOT NULL default 'y',
    description TEXT NOT NULL,
    http_method TEXT NOT NULL default 'GET',
    http_header TEXT,
    payload TEXT,
    data TEXT,
    url TEXT,
    response_code INTEGER NOT NULL default 200,
    response_header TEXT,
    validation_type TEXT,
    assertion TEXT,
    maxtime REAL
);
COMMIT;