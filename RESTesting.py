#!/usr/bin/env python
# -*- coding: utf8 -*-
"""Missing docstring"""

import logging
import argparse
import sys

from testrunner.validation.CheckArguments import CheckArguments
from testrunner.validation.CheckFile import CheckFile
from testrunner.connectors.ReadConfiguration import ReadConfiguration
from testrunner.connectors.ReadTestsuite import ReadTestsuite
from testrunner.RunTestSuite import RunTestSuite


class RESTesting(object):
    """Missing docstring"""

    def __init__(self):
        """
        Parse and assign arguments
        """
        # initialize logging
        self.__logger = logging.getLogger(__name__)
        self.__logger.debug('parse given arguments')

        # set argument description/epilog
        description = 'RESTesting automation by Python'
        epilog = 'Please read the README for detailed description!'
        parser = argparse.ArgumentParser(description=description,
                                         epilog=epilog)

        # set optional arguments
        parser.add_argument("-v", "--verbosity", action="count",
                            help="increase output verbosity")
        parser.add_argument("-o", "--output",
                            choices=['default', 'xml', 'json'],
                            help="set report output type")
        parser.add_argument("-i", "--ignore", action="count",
                            help="ignore SSL cert verification")
        parser.add_argument("-n", "--nofollow", action="count",
                            help="disable following redirects")
        parser.add_argument("-r", "--non_cacheable", action="count",
                            help="add random query string for GET")

        # set mandatory arguments
        parser.add_argument("config", help="the configuration file")
        parser.add_argument("host", help="the base host")
        parser.add_argument("suite", help="the sqlite test suite")

        # read arguments by user
        args = parser.parse_args()

        # set logging level
        if args.verbosity > 1:
            logging.basicConfig(level=logging.DEBUG)
        elif args.verbosity == 1:
            logging.basicConfig(level=logging.INFO)
        else:
            logging.basicConfig(level=logging.ERROR)

        self.__logger.debug(
            '\nConfiguration: %s\nHost: %s\nSuite: %s\nReport: %s\n',
            args.config, args.host, args.suite, args.output)

        # assign arguments
        self.__arg_path = ['', '']
        self.__arg_path[0] = args.config
        self.__arg_path[1] = args.suite

        self.__configuration = {}
        self.__host = args.host
        self.__testsuite = []
        self.__options = ['default', True, True, False]

        if args.output:
            self.__options[0] = args.output

        if args.ignore:
            self.__options[1] = False

        if args.nofollow:
            self.__options[2] = False

        if args.non_cacheable:
            self.__options[3] = True

    def __parse_configuration(self):
        """
        Check config argument
        """
        self.__logger.debug('parse configuration file')

        if not CheckArguments.is_empty_string(self.__arg_path[0]):
            self.__logger.critical('Empty string configuration')
            sys.exit(1)

        if not CheckFile.is_file_readable(self.__arg_path[0]):
            self.__logger.critical('Configuration not readable')
            sys.exit(1)

        if CheckFile.get_file_size(self.__arg_path[0]) == 0:
            self.__logger.critical('Configuration is empty')
            sys.exit(1)

        if ReadConfiguration.read_configuration_file(self.__arg_path[0]):
            self.__configuration = ReadConfiguration.get_configuration()
        else:
            sys.exit(1)

    def __parse_host(self):
        """
        Check host argument
        """
        self.__logger.debug('parse host')

        if not CheckArguments.is_empty_string(self.__host):
            self.__logger.critical('Empty string host')
            sys.exit(1)

        if not CheckArguments.is_reachable(self.__host):
            self.__logger.critical('Host not reachable')
            sys.exit(1)

    def __parse_testsuite(self):
        """
        Check suite argument
        """
        self.__logger.debug('parse testsuite')

        if not CheckArguments.is_empty_string(self.__arg_path[1]):
            self.__logger.critical('Empty string suite')
            sys.exit(1)

        if not CheckFile.is_file_readable(self.__arg_path[1]):
            self.__logger.critical('Testsuite not readable')
            sys.exit(1)

        if CheckFile.get_file_size(self.__arg_path[1]) == 0:
            self.__logger.critical('Testsuite is empty')
            sys.exit(1)

        if not CheckArguments.is_allowed_testsuite(self.__arg_path[1]):
            self.__logger.critical('Wrong testsuite extension')
            sys.exit(1)

        if ReadTestsuite.read_sqlite_file(self.__arg_path[1]):
            self.__testsuite = ReadTestsuite.get_testsuite()
        else:
            sys.exit(1)

        if len(self.__testsuite) == 0:
            self.__logger.critical('Found %d cases', len(self.__testsuite))
            sys.exit(1)

    def __parse_report(self):
        """
        Check output argument
        """
        self.__logger.debug('parse report')

        if not CheckArguments.is_output_value(self.__options[0]):
            self.__logger.critical('Report not allowed')
            sys.exit(1)

    def check_args(self):
        """
        Check mandatory arguments
        """
        self.__logger.info('** check given arguments **')

        self.__parse_configuration()
        self.__parse_host()
        self.__parse_testsuite()
        self.__parse_report()

    def start_test(self):
        """
        Start test run
        """
        test = RunTestSuite(self.__configuration,
                            self.__host,
                            self.__testsuite,
                            self.__options)
        test.run_tests()
        test.show_report()


if __name__ == '__main__':
    RUN = RESTesting()
    RUN.check_args()
    RUN.start_test()
